# UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO #
## FACULTAD DE ESTUDIOS SUPERIORES ACATLÁN ##

**MÉTODOS NUMÉRICOS II**

Grupo 2401


INTEGRANTES DEL EQUIPO:
> - Alcántara Sarmiento Fernando
> - Del Castillo Flores Ana Karen
> - Flores Zenteno Alfonso
> - González Briones Miguel Ariel

 - Entrega 1: Archivo ejecutable: **principal.exe** en la carpeta src/entrega1.
 - Entrega 2: Archivo ejecutable: **principal.exe** en la carpeta src/entrega2.
 - Entrega 3: Archivo ejecutable: **principal.exe** en la carpeta src/entrega3.


 - **Paquete Final**. Archivo ejecutable: **paquete.exe** en la carpeta src/Paquete. 