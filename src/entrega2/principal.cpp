#include <stdio.h>
#include <iostream>
#include <locale.h>
#include <cmath>
#define MAX  25

using namespace std;

float tabla[MAX][2];
int numValores = 0; //variable con la cantidad de datos proporcionados por el usuario
int gradoPol = 0;

void ingresaData(){
	int i, j, val, opc, pos,u,v;
	
	float x_1, y_1,rep,aux1,aux2;
	do
	{
		cout<< "\nCuantos valores quieres en X y Y:";
   		cin >> val;
   		numValores = val;
   		cout<< "El valor Ingresado es el Correcto? Si = 1, No = 2"<<endl;
   		cin>>rep;
   		system("cls");
   }
   while(rep!=1);
   rep=0;
 	
 	for(i=0;i<val;i++)
	{
		cout<<"X["<<i+1<<"]";
		cin>>tabla[i][0];
		cout<<"Y["<<i+1<<"]";
		cin>>tabla[i][1];
    	cout<<endl;
	}
	system("cls");
	cout << "\nX\t\tY\t\n";
	for(i=0;i<val;i++)
	{
		cout<<tabla[i][0]<<"\t\t"<<tabla[i][1];
    	cout<<endl;
    }
    cout<<"los Valores ingresados son los correctos? Si = 1, No = 2"<<endl;
    cin>>rep;
    if(rep==1)
    {
    	//Validar datos
    	//Ordenar datos por M. Burbuja
    	for(u=0;u<val;u++){
    		for(v=u+1;v<val;v++){
    			if(tabla[u][0] > tabla[v][0]){
    				aux1 = tabla[u][0];
    				tabla[u][0] = tabla[v][0];
    				tabla[v][0] = aux1;
    				aux2 = tabla[u][1];
    				tabla[u][1] = tabla[v][1];
    				tabla[v][1] = aux2;
				}
			}
		}
    	
	}
    else
    {
    	do
    	{
    	cout<<"Quieres Cambiar Algun Valor de  X o Y? X = 1  / Y = 2"<<endl;
    	cin>>opc;
    	system("cls");
    	switch(opc)
    	{
    		case 1:
    			do
    			{
					cout<<"Que Valor de X Quieres Cambiar? (Ingrese el �ndice)"<<endl;
					printf("\n i | x_i");
					for(i=0;i<val;i++){
						printf("\n %d | %f", i, tabla[i][0]);
					}
					
					cin>>pos;
					cout<<"Ingrese el Valor Correcto de X: "<<endl;
					cin>>x_1;
				
					tabla[pos][0]= x_1,
					
					system("cls");
					cout << "\nX\t\tY\t\n";
					for(i=0;i<val;i++)
					{
						cout<<tabla[i][0]<<"\t\t"<<tabla[i][1];
    					cout<<endl;
    				}
    			    cout<<"\nLos Valores Ingresados de X son Correctos? Si = 1, No = 2"<<endl;
    				cin>>rep;	
				}while(rep!=1);
				rep=0;
    			break;
    		case 2:
				do
    			{
					cout<<"Que Valor de Y Quieres Cambiar? (Ingrese el �ndice)"<<endl;
					printf("\n i | f(x_i)");
					for(i=0;i<val;i++){
						printf("\n %d | %f", i, tabla[i][1]);
					}
					cin>>pos;
					cout<<"Ingrese el Valor Correcto de Y: "<<endl;
					cin>>y_1;
					
					tabla[pos][1] = y_1;
					
					system("cls");
					cout << "\nX\t\tY\t\n";
					for(i=0;i<val;i++)
					{
						cout<<tabla[i][0]<<"\t\t"<<tabla[i][1];
    					cout<<endl;
    				}
    			    cout<<"\nLos Valores Ingresados son Correctos? Si = 1, No = 2"<<endl;
    				cin>>rep;	
				}while(rep!=1);
				rep=0;
    			break;
		}
		 cout<<"\nLos Valores Ingresados son Correctos? Si = 1, No = 2"<<endl;
    				cin>>rep;
    				
	}while(rep!=1);
	
	}	

};

class inter_newton {
	private:
		float x;
		float h;
		float s;
		int indice; //�ndice de la tabla en el que se empieza la interpolaci�n
		int grado; //grado del polinomio a crear
		int tipo; //Si es progresivo ser� 1, si es regresivo ser� 2
		float polinomio[20];//valores de polinomios por grados, l�mite 8
		float ese[20];//arreglo con el c�lculo de las eses para la iteraci�n
		float matrix[20][20];//tabla de diferencias divididas m�ximo para polinomios de tama�o 10
		int n; //numero total de valores a leer
	
	public:
	
		void setval(float a, float b, int c, int d, int e, int f){ //Pone los valores iniciales en las variables privadas
			x = a;
			h = b;
			indice = c;
			grado = d;
			tipo = e;
			n=f;
		};
		
		int fact(int x){
			if(x == 1 || x == 0){
				return 1;
			}else{
				return x*fact(x-1);
			}
		};
		
		void initialize(){
			//primero llenamos la tabla de diferencias, falta el uso de la tabla de valores dada por el usuario que ser� de ambito global
			int i,j,k,l,u,v;
			
			//Primero 'limpiamos' la matriz existente
			
			for(i=0;i<20;i++){
				for(j=0;j<20;j++){
					matrix[i][j] = 0.0;
				}
			}
			
			
			for(j=1;j<=n-1; j++){
				if(j==1){
					for(i=0;i<n-1;i++){
						matrix[i][j-1]=tabla[i+1][1]-tabla[i][1];
					}
				}else{
					for(i=0; i< n-j; i++){
						matrix[i][j-1]= matrix[i+1][j-2]-matrix[i][j-2];
					}
				}
			} 
		
			printf("\n\t > Tabla de diferencias \n");
			for(u=0;u<n-1;u++){//imprime tabla de diferencias
				for(v=0;v<n-1;v++){
					printf("| %.8f |",matrix[u][v]);
				}
				printf("\n\n");
			}
			
			if(tipo == 1){//caso progresivo
			
				s = x - tabla[indice][0];
				s = s / h;
				
				printf("\n\t s= %f\n",s);
				
				ese[0]=s;
				for(k=1;k<=grado;k++){//inicializa el vector de productos de s
					ese[k] = ese[k-1]*(s-k);
				}
				
				polinomio[0] = tabla[indice][1];
				for(l=1;l<=grado;l++){//inicializa y crea los polinomios
					polinomio[l] = polinomio[l-1] + (ese[l-1]*(matrix[indice][l-1]/fact(l)));
				}
				
			}else{
				if(tipo == 2){//caso regresivo
					s=(x-tabla[indice][0])/h;
					
					printf("\n\t s= %f\n",s);
					
					ese[0]=s;
					for(k=1;k<=grado;k++){//inicializa el vector de productos de s
						ese[k] = ese[k-1]*(s+k);
					}
					
					polinomio[0] = tabla[indice][1];//
					for(l=1;l<=grado;l++){
						polinomio[l] = polinomio[l-1] + (ese[l-1]*(matrix[indice-l][l-1]/fact(l)));
					}
					
				}
			}
		};
		
		double result(){
			return polinomio[grado];
		};
		
};

class splineCub {
	private :
		float H[20], F1[20], VR[20], VI[20], S[20], A[20], B[20], C[20];
		float tridiagonal[20][22], matrix_1[20][20], matrix_2[20][20];
	
	public :
		void pivote(int i,int j,int dim){
	
		float piv= matrix_1[i][j];
		int h,k;
	
		for(h=0;h<dim;h++){
			if(h != j){
			matrix_1[i][h]=(matrix_1[i][h]/piv)*-1;
			}
		}
	
		for(h=0;h<dim;h++){
			if(h!=i){
			for(k=0;k<dim;k++){
				if(k!=j){
				matrix_1[h][k]=matrix_1[h][k]+(matrix_1[i][k]*matrix_1[h][j]);
				}
			}
			}
		}
	
		for(k=0;k<dim;k++){
			if(k != i){
			matrix_1[k][j]=matrix_1[k][j]/piv;
			}
		}
	
		piv=1/piv;
		matrix_1[i][j]=piv;
	
		for(i=0;i<dim;i++){
	        for(j=0;j<dim;j++){
	            matrix_2[i][j]=matrix_1[i][j];
	        }
		}

		}

		void intercambio(int dim){
			int y,k,i,j;
			for(y=0;y<dim;y++){
				pivote(y,y,dim);
			}
		
//			printf("\n\nMatriz Inversa: \n\n");
		
		//	for(i=0;i<dim;i++){
		//        for(j=0;j<dim;j++){
		//            printf("%f ",matrix_2[i][j]);
		//        }
		//        printf("\n");
		//	}
		}
		
		void ajustarCurva(){
			int i,j,k,u,v,h;
			float total;
			
			for(i=0; i<numValores-1; i++){// llenado de vectores H Y F1
				H[i] = tabla[i+1][0]-tabla[i][0];
				F1[i] = (tabla[i+1][1]-tabla[i][1]) / H[i];
			}
			
			for(i=0; i<numValores-2; i++){//Llenado de la matriz tridiagonal
				tridiagonal[i][i] = H[i];
				tridiagonal[i][i+1] = 2 * (H[i]+H[i+1]);
				tridiagonal[i][i+2] = H[i+1];
			}
			
			printf("\n Matriz Tridiagonal: \n\n");
			for(j=0; j<numValores-2; j++){
				for(k=0; k<numValores; k++){
					printf("| %f |",tridiagonal[j][k]);
				}
				printf("\n");
			}
			
			for(j=0; j<numValores-2; j++){//recorte de la matriz tridiagonal
				for(k=0; k<numValores-2; k++){
					matrix_1[j][k] = tridiagonal[j][k+1];
				}
			}
			
			intercambio(numValores-2); //funciona invertir la matriz en matrix_2
			
			for(u=0; u<numValores-2; u++){//llenado de vector independiente
				VR[u] = (F1[u+1] - F1[u]) * 6;
			}
			
			
			for(u=0; u<numValores-2; u++){//multiplica matriz inversa por vector soluci�n para encontrar vector independiente VI
				total=0.0;
				for(v=0; v<numValores-2; v++){
					total += matrix_2[u][v] * VR[v];
				}
				VI[u] = total;
			}
			
			for(v=0;v<numValores-1; v++){ //reasigna valores calculados a vector S
				if(v==0){
					S[v] = 0;
				}else{
					S[v] = VI[v-1];
				}
			}
			
			printf("\n-----------------------------------------------------------------------------------------------------");
			
			for(i=0; i<numValores-1; i++){//Llenado de vectores A, B, C	
				A[i] = ( S[i+1] - S[i] ) / (6 * H[i]);
				B[i] = S[i] / 2;
				C[i] = F1[i] - ( ( ( S[i+1] + 2*S[i] ) / 6 ) * H[i] );
//				printf("\n| %f | %f | %f",A[i],B[i],C[i]);
			}
			
			printf("\n\t Splines C�bicos ");
			for(h=0;h<numValores-1;h++){
				printf("\n\n   > g(x_%d) = %f(x-x_%d)^3 + %f(x-x_%d)^2 + %f(x-x_%d) + %f  , para (%f,%f)",h,A[h],h,B[h],h,C[h],h,tabla[h][1],tabla[h][0],tabla[h+1][0]);
			}
			
		}
};

int main(){
	setlocale(LC_ALL,"");
	
	int opc,opc2=0,tipo, index, i,u, error=0;
	float valorX,hdiff,aux3,aux4;
	inter_newton sis1;
	splineCub sis2;
	
	    while(opc != 2){
    	system("cls");
    	printf("\t\t\t\t\tPrograma 2. Interpolaci�n y Ajuste de Curvas \t\t\t\t\n\n");
    	printf("\nGrupo 2401\nIntegrantes:\n\t>Alcntara Sarmiento Fernando\n\t>Del Castillo Flores Ana Karen\n\t>Flores Zenteno Alfonso\n\t>Gonzlez Briones Miguel Ariel\n\n\n");
        printf("1.- Ingresar tabla de valores \n\n");
        printf("2.- Salir\n\n");
    
		printf("Indica la Opcion a Evaluar: ");
        scanf("%d",&opc);
		
		if(opc == 1){
			ingresaData();
			printf("---------------------------------------------------------------------------------------");
			
			while(opc2 != 3){
				system("cls");
				printf("\nEscoge una opci�n: \n\t 1) Interpolar un valor \n\t 2) Ajustar una curva \n\t 3) Salir \n\t");
				scanf("%d",&opc2);
				
				switch(opc2){
					case 1:
						//Verifica datos igualmente espaciados
//						aux3 = tabla[1][0]-tabla[0][0];
//						aux4 = aux3;
//						for(u=0;u<numValores-1;u++){
//							if(tabla[u+1][0]-tabla[u][0] > aux3 || tabla[u+1][0]-tabla[u][0] < aux3)
//								aux4 = tabla[u+1][0]-tabla[u][0];
//						}
//						
//						aux4 = (aux4)/aux3;
//						
//						if(aux4 != 1){
//								printf("\n\n Los valores proporcionados no est�n igualmente espaciados !!");
//								printf("\n\n Por lo tanto no se puede interpolar por el m�todo de Newton Progresivo/Regresivo.\n\n");
//								system("pause");
//								error=1;
//							}
						
						
						if(error == 0){
							
							printf("\n\n\t > Dame el valor a interpolar: ");
							scanf("%f",&valorX);
							//validaci�n y saber si es progresivo o regresivo y obtener el �ndice del x cercano
							if(fabs(valorX-tabla[0][0]) > fabs(valorX-tabla[numValores-1][0])){
								tipo = 2;//para el caso regresivo
								for(i=0;i<numValores;i++){
									if(tabla[i][0]>valorX){
										index = i;
										break;
									}
								}
							}else{
								tipo = 1;//para el caso progresivo
								for(i=0;i<numValores;i++){
									if(tabla[i][0]<=valorX){
										index = i;
									}
								}
							}
						
							printf("\n\n\t > Dame el grado del polinomio: ");
							//validaci�n si se puede crear de ese grado
							scanf("%d",&gradoPol);
							if(tipo == 1){
								if(gradoPol>(numValores-index)){
									printf("\n\tEl grado del polinomio indicado no es compatible con el n�mero de datos ingresados.\n\tSe tomar� el mayor grado posible: %d",numValores-index);
									gradoPol = numValores-index;
								}
							}else{
								if(gradoPol>(index)){
									printf("\n\tEl grado del polinomio indicado no es compatible con el n�mero de datos ingresados.\n\tSe tomar� el mayor grado posible: %d",index);
									gradoPol = index;	
								}
							}
							printf("\n");
							
							hdiff = tabla[1][0] - tabla[0][0]; 
							
							sis1.setval(valorX, hdiff, index, gradoPol, tipo,numValores);
							sis1.initialize();
							printf("\n\n\t P_%d (%f) = %.8f",gradoPol,valorX,sis1.result());
							printf("\n");
							
							system("pause");
						}
					
					break;
					
					case 2:
						
						sis2.ajustarCurva();
						
						system("pause");
					break;
					
					default:
						printf("\n\n\t Gracias por usar el programa"); 
				}
			
			}
		
		}
        
    }
	
	return 0;
}
