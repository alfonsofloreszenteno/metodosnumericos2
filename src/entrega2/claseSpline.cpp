#include <stdio.h>

class splineCub {
	private :
		float H[20], F1[20], VR[20], VI[20], S[20], A[20], B[20], C[20];
		float tridiagonal[20][22], matrix_1[20][20], matrix_2[20][20];
	
	public :
		void pivote(int i,int j,int dim){
	
		float piv= matrix_1[i][j];
		int h,k;
	
		for(h=0;h<dim;h++){
			if(h != j){
			matrix_1[i][h]=(matrix_1[i][h]/piv)*-1;
			}
		}
	
		for(h=0;h<dim;h++){
			if(h!=i){
			for(k=0;k<dim;k++){
				if(k!=j){
				matrix_1[h][k]=matrix_1[h][k]+(matrix_1[i][k]*matrix_1[h][j]);
				}
			}
			}
		}
	
		for(k=0;k<dim;k++){
			if(k != i){
			matrix_1[k][j]=matrix_1[k][j]/piv;
			}
		}
	
		piv=1/piv;
		matrix_1[i][j]=piv;
	
		for(i=0;i<dim;i++){
	        for(j=0;j<dim;j++){
	            matrix_2[i][j]=matrix_1[i][j];
	        }
		}

		}

		void intercambio(int dim){
			int y,k,i,j;
			for(y=0;y<dim;y++){
				pivote(y,y,dim);
			}
		
			printf("\n\nMatriz Inversa: \n\n");
		
		//	for(i=0;i<dim;i++){
		//        for(j=0;j<dim;j++){
		//            printf("%f ",matrix_2[i][j]);
		//        }
		//        printf("\n");
		//	}
		}
		
		void ajustarCurva(){
			int i,j,k,u,v;
			float total;
			
			for(i=0; i<6; i++){// llenado de vectores H Y F1
				H[i] = tabla[i+1][0]-tabla[i][0];
				F1[i] = (tabla[i+1][1]-tabla[i][1]) / H[i];
			}
			
			for(i=0; i<5; i++){//Llenado de la matriz tridiagonal
				tridiagonal[i][i] = H[i];
				tridiagonal[i][i+1] = 2 * (H[i]+H[i+1]);
				tridiagonal[i][i+2] = H[i+1];
			}
			
			printf("\n Matriz Tridiagonal: \n\n");
			for(j=0; j<5; j++){
				for(k=0; k<7; k++){
					printf("| %f |",tridiagonal[j][k]);
				}
				printf("\n");
			}
			
			for(j=0; j<5; j++){//recorte de la matriz tridiagonal
				for(k=0; k<5; k++){
					matrix_1[j][k] = tridiagonal[j][k+1];
				}
			}
			
			intercambio(5); //funciona invertir la matriz en matrix_2
			
			for(u=0; u<5; u++){//llenado de vector independiente
				VR[u] = (F1[u+1] - F1[u]) * 6;
			}
			
			
			for(u=0; u<5; u++){//multiplica matriz inversa por vector soluci�n para encontrar vector independiente VI
				total=0.0;
				for(v=0; v<5; v++){
					total += matrix_2[u][v] * VR[v];
				}
				VI[u] = total;
			}
			
			for(v=0;v<6; v++){ //reasigna valores calculados a vector S
				if(v==0){
					S[v] = 0;
				}else{
					S[v] = VI[v-1];
				}
			}
			
			printf("\n-------------------------------------------------------------");
			
			for(i=0; i<6; i++){//Llenado de vectores A, B, C	
				A[i] = ( S[i+1] - S[i] ) / (6 * H[i]);
				B[i] = S[i] / 2;
				C[i] = F1[i] - ( ( ( S[i+1] + 2*S[i] ) / 6 ) * H[i] );
			}
		}
};
