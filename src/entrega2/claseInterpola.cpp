#include <stdio.h>

class inter_newton {
	private:
		float x;
		float h;
		float s;
		int indice; //�ndice de la tabla en el que se empieza la interpolaci�n
		int grado; //grado del polinomio a crear
		int tipo; //Si es progresivo ser� 1, si es regresivo ser� 2
		float polinomio[8];//valores de polinomios por grados, l�mite 8
		float ese[8];//arreglo con el c�lculo de las eses para la iteraci�n
		float matrix[10][10];//tabla de diferencias divididas m�ximo para polinomios de tama�o 10
		
		float tabla[6][2]={{50,24.94},{60,30.11},{70,36.05},{80,42.84},{90,50.57},{100,59.30}};
		int n; //numero total de valores a leer
	
	public:
	
		void setval(float a, float b, int c, int d, int e, int f){ //Pone los valores iniciales en las variables privadas
			x = a;
			h = b;
			indice = c;
			grado = d;
			tipo = e;
			n=f;
		};
		
		void initialize(){
			//primero llenamos la tabla de diferencias, falta el uso de la tabla de valores dada por el usuario que ser� de ambito global
			int i,j,k,l,u,v;
			
			//Primero 'limpiamos' la matriz existente
			
			for(i=0;i<10;i++){
				for(j=0;j<10;j++){
					matrix[i][j] = 0.0;
				}
			}
			
			
			for(j=1;j<=n-1; j++){
				if(j==1){
					for(i=0;i<n-1;i++){
						matrix[i][j-1]=tabla[i+1][1]-tabla[i][1];
					}
				}else{
					for(i=0; i< n-j; i++){
						matrix[i][j-1]= matrix[i+1][j-2]-matrix[i][j-2];
					}
				}
			} 
			
//			for(j=0;j<n-1;j++)
//			{
//				for(i=0;i<n-1;i++)
//				{
//					if(j==0)
//					{
//						matrix[i][j]=tabla[i+1][1]-tabla[i][1];
//					}
//					else 
//					{
//						if(i<k)
//							matrix[i][j]=matrix[i+1][j-1]-matrix[i][j-1];
//					}
//
//				}
//				k-=1;
//			}
			
			//M�TODO DEL PRIMO ARRIBA**
			
			for(u=0;u<n-1;u++){//imprime tabla de diferencias
				for(v=0;v<n-1;v++){
					printf("| %.4f |",matrix[u][v]);
				}
				printf("\n\n");
			}
			
			if(tipo == 1){//caso progresivo
			
				s = x - tabla[indice][0];
				s = s / h;
				
				printf("\n\t s= %f\n",s);
				
				ese[0]=s;
				for(k=1;k<=grado;k++){//inicializa el vector de productos de s
					ese[k] = ese[k-1]*(s-k);
				}
				
				polinomio[0] = tabla[indice][1];
				for(l=1;l<=grado;l++){//inicializa y crea los polinomios
					polinomio[l] = polinomio[l-1] + (ese[l-1]*(matrix[indice][l-1]/fact(l)));
				}
				
			}else{
				if(tipo == 2){//caso regresivo
					s=(x-tabla[indice][0])/h;
					
					ese[0]=s;
					for(k=1;k<=grado;k++){//inicializa el vector de productos de s
						ese[k] = ese[k-1]*(s+k);
					}
					
					polinomio[0] = tabla[indice][1];//
					for(l=1;l<=grado;l++){
						polinomio[l] = polinomio[l-1] + (ese[l-1]*(matrix[grado+1-l][l-1]/fact(l)));
					}
					
				}
			}
		};
		
		int fact(int x){
			if(x == 1 || x == 0){
				return 1;
			}else{
				return x*fact(x-1);
			}
		};
		
		double result(){
			return polinomio[grado];
		};
		
};
