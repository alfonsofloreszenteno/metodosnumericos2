#include <stdio.h>
#include <math.h>

class nrf3_1 {
	private:
		double x, y,z;
		double matrizf31[3][3];
		double evalf31[3];
		double minversa[9];
		double cofact[3][3];
		double adjt[3][3];
		double determ;

	public:
		
		void setval(double v,double w,double h){
		x=v;
		y=w;
		z=h;
		};
		
		void jacobiana(){
			matrizf31[0][0]=(4*x)-4;
			matrizf31[0][1]=2*y;
			matrizf31[0][2]=(6*z)+6;
			matrizf31[1][0]=2*x;
			matrizf31[1][1]=(2*y)-2;
			matrizf31[1][2]=4*z;
			matrizf31[2][0]=(6*x)-12;
			matrizf31[2][1]=2*y;
			matrizf31[2][2]=-6*z;
		};
		
		void evaluar(){
			evalf31[0]= (2*pow(x,2))-(4*x)+pow(y,2)+(3*pow(z,2))+(6*z)+2;
			evalf31[1]= pow(x,2)+pow(y,2)-(2*y)+(2*pow(z,2))-5;
			evalf31[2]= (3*pow(x,2))-(12*x)+pow(y,2)-(3*pow(z,2))+8;
		};
		
		double determinante(){
			double sum, rest;
			sum =(matrizf31[0][0]*matrizf31[1][1]*matrizf31[2][2])+(matrizf31[1][0]*matrizf31[2][1]*matrizf31[0][2])+(matrizf31[2][0]*matrizf31[0][1]*matrizf31[1][2]);
			rest=(matrizf31[0][2]*matrizf31[1][1]*matrizf31[2][0])+(matrizf31[1][2]*matrizf31[2][1]*matrizf31[0][0])+(matrizf31[2][2]*matrizf31[0][1]*matrizf31[1][0]);
			determ=sum-rest;
			return determ;
		}
		
		void cofactores(){
			cofact[0][0]=(matrizf31[1][1]*matrizf31[2][2])-(matrizf31[1][2]*matrizf31[2][1]);
			cofact[0][1]=-1*((matrizf31[1][0]*matrizf31[2][2])-(matrizf31[1][2])*matrizf31[2][0]);
			cofact[0][2]=(matrizf31[1][0]*matrizf31[2][1])-(matrizf31[1][1]*matrizf31[2][0]);
			cofact[1][0]=-1*((matrizf31[0][1]*matrizf31[2][2])-(matrizf31[0][2]*matrizf31[2][1]));
			cofact[1][1]=(matrizf31[0][0]*matrizf31[2][2])-(matrizf31[0][2]*matrizf31[2][0]);
			cofact[1][2]=-1*((matrizf31[0][0]*matrizf31[2][1])-(matrizf31[0][1]*matrizf31[2][0]));
			cofact[2][0]=(matrizf31[0][1]*matrizf31[1][2])-(matrizf31[0][2]*matrizf31[1][1]);
			cofact[2][1]=-1*((matrizf31[0][0]*matrizf31[1][2])-(matrizf31[0][2]*matrizf31[1][0]));
			cofact[2][2]=(matrizf31[0][0]*matrizf31[1][1])-(matrizf31[0][1]*matrizf31[1][0]);
		}
		
		void adjunta(){
			adjt[0][0]=cofact[0][0];
			adjt[0][1]=cofact[1][0];
			adjt[0][2]=cofact[2][0];
			adjt[1][0]=cofact[0][1];
			adjt[1][1]=cofact[1][1];
			adjt[1][2]=cofact[2][1];
			adjt[2][0]=cofact[0][2];
			adjt[2][1]=cofact[1][2];
			adjt[2][2]=cofact[2][2];
		}
		
		
		
		
		double *getjacob_inv(){
			determinante();
			cofactores();
			adjunta();
			
			minversa[0]=adjt[0][0]/determ;
			minversa[1]=adjt[0][1]/determ;
			minversa[2]=adjt[0][2]/determ;
			minversa[3]=adjt[1][0]/determ;
			minversa[4]=adjt[1][1]/determ;
			minversa[5]=adjt[1][2]/determ;
			minversa[6]=adjt[2][0]/determ;
			minversa[7]=adjt[2][1]/determ;
			minversa[8]=adjt[2][2]/determ;
			return &minversa[0];
		};
		
		double *geteval(){
			return &evalf31[0];
			};
		
		void imprime(){
			int l,j,k=0;
			
			printf("\n             |Jacobiana^-1              | F(X^k-1)|\n");
			for(l=0;l<3;l++){
				printf("\n");
				for(j=0;j<3;j++){
					printf("| %.9lf |",minversa[k]);
					k++;
				}
				printf(" | %.9lf |\n",evalf31[l]);
			}
			
		};
		
};


