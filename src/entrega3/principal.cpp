#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<conio.h>

double limit_val_fx[100];
int num;
double h;
int numValores=0;
double X[50];
double FX[50];



double Simpson13(){ //////////calculo de Simpson 1/3
	
  int i;
  double segundasum=0,tercerasum=0;
  double primerasum=(limit_val_fx[0]+limit_val_fx[num]);
  for(i=1;i<=num-1;i++){
    if(i%2==0)
        segundasum+=limit_val_fx[i+1];
    else
        tercerasum+=limit_val_fx[i];
  }
  return ((h/3)*(primerasum+((4*tercerasum)+(2*segundasum))));
}
double Simpson38(){ ////////////// calculo de Simpson 3/8
	
  int i;
  double segundasum=0,tercerasum=0;
  double primerasum=(limit_val_fx[0]+limit_val_fx[num]);
  for(i=1;i<=num-1;i++){
    if(i%3==0)
        segundasum+=limit_val_fx[i];
    else
        tercerasum+=limit_val_fx[i];
  }
  return 3*h/8*(primerasum+2*segundasum+3*tercerasum);
} 

int main(){
	
	double nume,a,b,h1, y; 
	int i=0, resp, pos;
	double x;
	
	do
	{
		
	printf("\t\t\tTercer Programa de Metodos Numericos 2\n\n");
	do
	{
	printf("Ingrese el Limite Superior\n"); //////////Limite superior de La integral
	scanf("%lf",&b);
	printf("El Valor Es Correcto?\nSi = 1\nNo = 2\n");
	scanf("%d",&resp);
	}while(resp!=1);
	system("cls");
	do
	{
	printf("Ingrese el Limite Inferior\n"); ///////////Limite inferior de la integral 
	scanf("%lf",&a);
	printf("El Valor Es Correcto?\nSi = 1\nNo = 2\n");
	scanf("%d",&resp);
	}while(resp!=1);
	system("cls");
	
	
	do
	{
	printf("Ingrese La H: "); ///////////se pide la h
	scanf("%lf",&h1);
	printf("El Valor Es Correcto?\nSi = 1\nNo = 2\n");
	scanf("%d",&resp);
	}while(resp!=1);
	system("cls");

	
	h=h1;
	i=0;
	for(nume=a;nume<=b;nume+=h) ///////////se hace el ingremento con la H
	{
		X[i]=nume;
		i++;
	}
	numValores=i;
	num=i+2;
	
	for(i=0;i<=numValores;i++){//verifica que los 'x' est�n bien calculados
		printf(" - %.3lf \n",X[i]);
	}
	
	
	double x0=a; ///////////////se ingresan los valores de F(x)
	printf("%d",num);
	double fx[num];
	
	printf("Ingrese Los Valores De F(x)\n");
	for(i=0;i<numValores;i++)
	{
		printf("f(%.2lf) : ",X[i]);
		scanf("%lf",&FX[i]);
	}
	system("cls");
	do
	{

		printf("Los Valores Ingresados de F(x)\n");
		for(i=0;i<numValores;i++)
		{
			printf("f(%.2lf): %.6lf\n",X[i], FX[i]);
		}
		printf("Desea Cambiar Un Valor?\nSi = 1 No = 2\n");
		scanf("%d",&resp);
		if(resp!=2)
		{
			printf("Que Valor Desea Cambiar? ");
			scanf("%d",&pos);
			printf("Ingrese El Dato Correcto: ");
			scanf("%lf",&y);
			FX[pos-1]=y;
		}
		system("cls");
		
	}while(resp!=2); 
	
	////////// se vacian los valores en otro vector para hacer el calculo (aunque podemos ucapar el mismo vector sin tener que usar otro..)
	
	limit_val_fx[0]=fx[0];
    
    for(i=0;i<num;i++)
    {
    	 limit_val_fx[i]=fx[i];
	}
	
	for(i=0;i<numValores;i++) ////////// se imprime la tabla
	{
		printf("x: %0.2lf \t\t f(%.2lf) %.6lf\n",X[i],X[i],FX[i]);
	}
	
	double simpson1=Simpson13();
    printf("\nLa Regla de Simpson de 1/3 es = %lf",simpson1);
    double simpson3=Simpson38();
    printf("\nLa Regla de Simpson de 3/8 es = %lf",simpson3);
    printf("\nDesea Intentar Otra Vez?\nSi=1\nNo=2\n");
    scanf("%d",&resp);
	}while(resp!=1);
	return 0;
}
