#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <locale.h>
#include "nrf2_1.cpp"
#include "nrf2_2.cpp"
#include "nrf3_1.cpp"
#include "nrf3_2.cpp"

float tolerancia;
int itera_max;
int confirma;

double max2(double a,double b){
	if(fabs(a)>fabs(b)){
		return fabs(a);
	}else{
		return fabs(b);
	}
}

double max3(double a, double b, double c){
	if(fabs(a)>fabs(b)&&fabs(a)>fabs(c)){ return fabs(a);}
	if(fabs(b)>fabs(a)&&fabs(b)>fabs(c)){ return fabs(b);}
	if(fabs(c)>fabs(a)&&fabs(c)>fabs(b)){ return fabs(c);}
}

void sistema1(){
	int i;
	double x,y,xa,ya;
	double *mat, *fun;
	double norma=10;
				do{
					system("cls");
					printf("Ingrese el Valor Inicial x: \n");
                    scanf("%lf",&x);
                    printf("Ingrese el Valor Inicial y: \n");
                    scanf("%lf",&y);
                    printf("Ingrese el numero de Iteraciones a Evaluar: \n");
                    scanf("%d",&itera_max);
                    printf("Ingrese la Tolerancia: \n");
                    scanf("%lf",&tolerancia);
                    //Aqui va el desarrollo del sistema 1//
                    nrf2_1 sol21;
          					
          				printf("\n");
						for(i=0;i<itera_max;i++){
							sol21.setval(x,y);
		                    sol21.jacobiana();
		                    sol21.evaluar();
		                    
							if(sol21.determinante()==0){
	                    	printf("\n\tEl determinante de la matriz jacobiana es igual a cero, por lo tanto no se puede calcular la inversa.\n\tIntente con otros valores.");
	                    	break;
							}
							
							if(norma<tolerancia){
								printf("\nSe ha alcanzado la tolerancia en la Iteracion %d",i);
								break;
							}	
							
							printf("\nIteracion %d-------------------------------------------------------------------\n",i+1);
							
							mat = sol21.getjacob_inv();
							fun = sol21.geteval();
							xa=x;
							ya=y;
							
							
							x=xa-( *(fun)*( *(mat)) + (*(fun+1))*(*(mat+1)) );
							y=ya-( *(fun)*( *(mat+2))  +  *(fun+1)*(*(mat+3))  );
							norma=max2(x-xa,y-ya);

							sol21.imprime();
							
							printf("\nX(%d)= %.9lf",i+1,x);
							printf("\nY(%d)= %.9lf \n",i+1,y);
							printf("\n\t\t\t\t-norma=%.9lf",norma);
						}
						
                    printf("\n\nDesea realizar el sistema otra vez Si=1\nNo=2\n");
                    scanf("%d",&confirma);
                }while (confirma==1);
}

void sistema2(){
	int i;
	double x,y,xa,ya;
	double *mat, *fun;
	double norma=10;
					do{
					system("cls");
					printf("Ingrese el Valor Inicial x: \n");
                    scanf("%lf",&x);
                    printf("Ingrese el Valor Inicial y: \n");
                    scanf("%lf",&y);
                    printf("Ingrese el numero de Iteraciones a Evaluar: \n");
                    scanf("%d",&itera_max);
                    printf("Ingrese la Tolerancia: \n");
                    scanf("%lf",&tolerancia);
                    
                    
                    nrf2_2 sol22;
          					
          				printf("\n");
						for(i=0;i<itera_max;i++){
							sol22.setval(x,y);
		                    sol22.jacobiana();
		                    sol22.evaluar();
		                    
							if(sol22.determinante()==0){
	                    	printf("\n\tEl determinante de la matriz jacobiana es igual a cero, por lo tanto no se puede calcular la inversa.\n\tIntente con otros valores.");
	                    	break;
							}
							
							if(norma<tolerancia){
								printf("\nSe ha alcanzado la tolerancia en la Iteracion %d",i);
								break;
							}	
							
							printf("\nIteracion %d-------------------------------------------------------------------\n",i+1);
							
							mat = sol22.getjacob_inv();
							fun = sol22.geteval();
							xa=x;
							ya=y;
							
							
							x=xa-( *(fun)*( *(mat)) + (*(fun+1))*(*(mat+1)) );
							y=ya-( *(fun)*( *(mat+2))  +  *(fun+1)*(*(mat+3))  );
							norma=max2(x-xa,y-ya);
							
							sol22.imprime();
							
							printf("\nX(%d)= %.9lf",i+1,x);
							printf("\nY(%d)= %.9lf \n",i+1,y);
							printf("\n\t\t\t\t-norma=%.9lf",norma);
						}
                    
                    printf("\n\nDesea realizar el sistema otra vez \n Si=1\nNo=2\n");
                    scanf("%d",&confirma);
                }while (confirma==1);
}

void sistema3(){
	int i;
	float x,y,xa,ya,z,za;
	float *mat, *fun;
	float norma=10;
					do{
					system("cls");
					printf("Ingrese el Valor Inicial x: \n");
                    scanf("%f",&x);
                    printf("Ingrese el Valor Inicial y: \n");
                    scanf("%f",&y);
                    printf("Ingrese el Valor Inicial z: \n");
                    scanf("%f",&z);
                    printf("Ingrese el numero de Iteraciones a Evaluar: \n");
                    scanf("%d",&itera_max);
                    printf("Ingrese la Tolerancia: \n");
                    scanf("%f",&tolerancia);
                    
                    
                    nrf3_1 sol31;
          					
          				printf("\n");
						for(i=0;i<itera_max;i++){
							sol31.setval(x,y,z);
		                    sol31.jacobiana();
		                    sol31.evaluar();
		                    sol31.invertir();
		                    
							if(sol31.determinante()==0){
	                    	printf("\n\tEl determinante de la matriz jacobiana es igual a cero, por lo tanto no se puede calcular la inversa.\n\tIntente con otros valores.");
	                    	break;
							}
							
							if(norma<tolerancia){
								printf("\nSe ha alcanzado la tolerancia en la Iteracion %d",i);
								break;
							}
							
							printf("\nIteracion %d-------------------------------------------------------------------\n",i+1);
							
							mat = sol31.getjacob_inv();
							fun = sol31.geteval();
							xa=x;
							ya=y;
							za=z;
							
							x=xa-( *(fun)*( *(mat)) + (*(fun+1))*(*(mat+1)) + *(fun+2)*( *(mat+2)));
							y=ya-( *(fun)*( *(mat+3))  +  *(fun+1)*(*(mat+4)) + *(fun+2)*( *(mat+5)) );
							z=za-( *(fun)*( *(mat+6)) +*(fun+1)*( *(mat+7)) + *(fun+2)*( *(mat+8)) );
							
							norma=max3(x-xa,y-ya,z-za);
							
							sol31.imprime();
							
							printf("\nX(%d)= %.6f",i+1,x);
							printf("\nY(%d)= %.6f",i+1,y);
							printf("\nZ(%d)= %.6f \n",i+1,z);
							printf("\n\t\t\t\t-norma=%.6f",norma);
						}
                    
                    
                    printf("\n\nDesea realizar el sistema otra vez Si=1\nNo=2\n");
                    scanf("%d",&confirma);
                }while (confirma==1);
}

void sistema4(){
		int i;
	double x,y,xa,ya,z,za;
	double *mat, *fun;
	double norma=10;
					do{
					system("cls");
					printf("Ingrese el Valor Inicial x: \n");
                    scanf("%lf",&x);
                    printf("Ingrese el Valor Inicial y: \n");
                    scanf("%lf",&y);
                    printf("Ingrese el Valor Inicial z: \n");
                    scanf("%lf",&z);
                    printf("Ingrese el numero de Iteraciones a Evaluar: \n");
                    scanf("%d",&itera_max);
                    printf("Ingrese la Tolerancia: \n");
                    scanf("%lf",&tolerancia);
                    
                    
                    nrf3_2 sol32;
          					
          				printf("\n");
						for(i=0;i<itera_max;i++){
							sol32.setval(x,y,z);
		                    sol32.jacobiana();
		                    sol32.evaluar();
		                    
							if(sol32.determinante()==0){
	                    	printf("\n\tEl determinante de la matriz jacobiana es igual a cero, por lo tanto no se puede calcular la inversa.\n\tIntente con otros valores.");
	                    	break;
							}
							
							if(norma<tolerancia){
								printf("\nSe ha alcanzado la tolerancia en la Iteracion %d",i);
								break;
							}
							
							printf("\nIteracion %d-------------------------------------------------------------------\n",i+1);
							
							mat = sol32.getjacob_inv();
							fun = sol32.geteval();
							xa=x;
							ya=y;
							za=z;
							
							x=xa-( *(fun)*( *(mat)) + (*(fun+1))*(*(mat+1)) + *(fun+2)*( *(mat+2)));
							y=ya-( *(fun)*( *(mat+3))  +  *(fun+1)*(*(mat+4)) + *(fun+2)*( *(mat+5)) );
							z=za-( *(fun)*( *(mat+6)) +*(fun+1)*( *(mat+7)) + *(fun+2)*( *(mat+8)) );
							
							norma=max3(x-xa,y-ya,z-za);
							
							sol32.imprime();
							
							printf("\nX(%d)= %.9lf",i+1,x);
							printf("\nY(%d)= %.9lf ",i+1,y);
							printf("\nZ(%d)= %.9lf \n",i+1,z);
							printf("\n\t\t\t\t-norma=%.9lf",norma);
						}
                    
                    
                    printf("\n\nDesea realizar el sistema otra vez Si=1\nNo=2\n");
                    scanf("%d",&confirma);
                }while (confirma==1);
}

int entregaUno()
{
	setlocale(LC_ALL,"");
    int p=0;
    int opc;
    int val1,val2,iter;
    double  x,y,z,tol;

    while(opc!=5){
    	system("cls");
    	printf("\t\t\t\t\tPrograma 1. Metodo de Newton Raphson\t\t\t\t\n\n");
    	printf("1.- F1(x,y)= x^2+xy-10=0 \t\t F2(x,y)= y+3xy^2-50=0\n\n");
        printf("2.- F1(x,y)= x^2+y^2-9=0 \t\t F2(x,y)= -e^x-2y-3=0\n\n");
        printf("3.- F1(x,y,z)= 2x^2-4x+y^2+3z^2+6z+2=0 \t F2(x,y)= x^2+y^2-2y+2z^2-5=0\t\t F3(x,y,z)= 3x^2-12x+y^2-3z^2+8=0\n\n");
        printf("4.- F1(x,y,z)= x^2-4x+y^2=0 \t\t F2(x,y,z)= x^2-x-12y+1=0 \t\t F3(x,y,z)= 3x^2-12x+y^2-3z^2+8=0\n\n");
        printf("5.- Salir\n\n");
        
		printf("Indica la Opcion a Evaluar: ");
        scanf("%d",&opc);
        switch(opc)
        {
            case 1: sistema1();
			    break;
            case 2: sistema2();
			    break;
            case 3: sistema3();
			    break;
            case 4:  sistema4();
				break;
			case 5:
				break;
        }
    }
    
    return 0;
}
