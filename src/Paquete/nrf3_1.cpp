#include <stdio.h>
#include <math.h>

class nrf3_1 {
	private:
		float x, y,z;
		float matrizf31[3][3];
		float evalf31[3];
		float minversa[9];
		//float cofact[3][3];
		float matrix_2[3][3];
		float matrix_1[3][3];
		//float adjt[3][3];
		float determ;

	public:
		
		void setval(float v,float w,float h){
		x=v;
		y=w;
		z=h;
		};
		
		void jacobiana(){
			matrizf31[0][0]=(4*x)-4;
			matrizf31[0][1]=2*y;
			matrizf31[0][2]=(6*z)+6;
			matrizf31[1][0]=2*x;
			matrizf31[1][1]=(2*y)-2;
			matrizf31[1][2]=4*z;
			matrizf31[2][0]=(6*x)-12;
			matrizf31[2][1]=2*y;
			matrizf31[2][2]=-6*z;
		};
		
		
		void pivote(int i,int j,int dim){
	
		float piv= matrix_1[i][j];
		int h,k;
	
		for(h=0;h<dim;h++){
			if(h != j){
			matrix_1[i][h]=(matrix_1[i][h]/piv)*-1;
			}
		}
	
		for(h=0;h<dim;h++){
			if(h!=i){
			for(k=0;k<dim;k++){
				if(k!=j){
				matrix_1[h][k]=matrix_1[h][k]+(matrix_1[i][k]*matrix_1[h][j]);
				}
			}
			}
		}
	
		for(k=0;k<dim;k++){
			if(k != i){
			matrix_1[k][j]=matrix_1[k][j]/piv;
			}
		}
	
		piv=1/piv;
		matrix_1[i][j]=piv;
	
		for(i=0;i<dim;i++){
	        for(j=0;j<dim;j++){
	            matrix_2[i][j]=matrix_1[i][j];
	        }
		}

		}

		
		void intercambio(int dim){
			int y;
			for(y=0;y<dim;y++){
				pivote(y,y,dim);
			}
		
		}
		
		
		void evaluar(){
			evalf31[0]= (2*pow(x,2))-(4*x)+pow(y,2)+(3*pow(z,2))+(6*z)+2;
			evalf31[1]= pow(x,2)+pow(y,2)-(2*y)+(2*pow(z,2))-5;
			evalf31[2]= (3*pow(x,2))-(12*x)+pow(y,2)-(3*pow(z,2))+8;
		};
	
		
		float determinante(){
			float sum, rest;
			sum =(matrizf31[0][0]*matrizf31[1][1]*matrizf31[2][2])+(matrizf31[1][0]*matrizf31[2][1]*matrizf31[0][2])+(matrizf31[2][0]*matrizf31[0][1]*matrizf31[1][2]);
			rest=(matrizf31[0][2]*matrizf31[1][1]*matrizf31[2][0])+(matrizf31[1][2]*matrizf31[2][1]*matrizf31[0][0])+(matrizf31[2][2]*matrizf31[0][1]*matrizf31[1][0]);
			determ=sum-rest;
			return determ;
		}
		
//		void cofactores(){
//			cofact[0][0]=(matrizf31[1][1]*matrizf31[2][2])-(matrizf31[1][2]*matrizf31[2][1]);
//			cofact[0][1]=-1*((matrizf31[1][0]*matrizf31[2][2])-(matrizf31[1][2])*matrizf31[2][0]);
//			cofact[0][2]=(matrizf31[1][0]*matrizf31[2][1])-(matrizf31[1][1]*matrizf31[2][0]);
//			cofact[1][0]=-1*((matrizf31[0][1]*matrizf31[2][2])-(matrizf31[0][2]*matrizf31[2][1]));
//			cofact[1][1]=(matrizf31[0][0]*matrizf31[2][2])-(matrizf31[0][2]*matrizf31[2][0]);
//			cofact[1][2]=-1*((matrizf31[0][0]*matrizf31[2][1])-(matrizf31[0][1]*matrizf31[2][0]));
//			cofact[2][0]=(matrizf31[0][1]*matrizf31[1][2])-(matrizf31[0][2]*matrizf31[1][1]);
//			cofact[2][1]=-1*((matrizf31[0][0]*matrizf31[1][2])-(matrizf31[0][2]*matrizf31[1][0]));
//			cofact[2][2]=(matrizf31[0][0]*matrizf31[1][1])-(matrizf31[0][1]*matrizf31[1][0]);
//		}
//		
//		void adjunta(){
//			adjt[0][0]=cofact[0][0];
//			adjt[0][1]=cofact[1][0];
//			adjt[0][2]=cofact[2][0];
//			adjt[1][0]=cofact[0][1];
//			adjt[1][1]=cofact[1][1];
//			adjt[1][2]=cofact[2][1];
//			adjt[2][0]=cofact[0][2];
//			adjt[2][1]=cofact[1][2];
//			adjt[2][2]=cofact[2][2];
//		}
//		
		
		void invertir(){
			int i,j;
			for(i=0;i<3;i++){
				for(j=0;j<3;j++){
					matrix_1[i][j]= matrizf31[i][j];
				}
			}
			
			intercambio(3);	
		}
		
		float *getjacob_inv(){ //No s� por qu� es de tipo apuntador si al final todo queda en las matrices globales.
		
			minversa[0]=matrix_2[0][0];
			minversa[1]=matrix_2[0][1];
			minversa[2]=matrix_2[0][2];
			minversa[3]=matrix_2[1][0];
			minversa[4]=matrix_2[1][1];
			minversa[5]=matrix_2[1][2];
			minversa[6]=matrix_2[2][0];
			minversa[7]=matrix_2[2][1];
			minversa[8]=matrix_2[2][2];
			
			return &minversa[0];
		};
		
		float *geteval(){
			return &evalf31[0];
			};
		
		void imprime(){
			int l,j,k=0;
			
			printf("\n             |Jacobiana^-1              | F(X^k-1)|\n");
			for(l=0;l<3;l++){
				printf("\n");
				for(j=0;j<3;j++){
					printf("| %f |",minversa[k]);
					k++;
				}
				printf(" | %f |\n",evalf31[l]);
			}
			
		};
		
};


