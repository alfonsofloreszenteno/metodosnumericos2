#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

float X[40];
float FX[40];
float result[40];
float H;
int numValores2;
int numPaneles;
float integral;
void reset();
void integra();
void deriva();
float simpson13(int a, int b);
float simpson38(int a, int b);
void derivarInt(int a, int b);


void integra(){
	if(numPaneles%2 == 0){
		//Simpson 1/3
		integral = simpson13(0,numPaneles);
	}else{
		if(numPaneles <= 5){
			//Simpson 3/8
			integral = simpson38(0,numPaneles);
		}else{
			//combina ambos, 
			integral = simpson13(0,numPaneles-2) + simpson38(numPaneles-2,numPaneles);
		}
	}
}

float simpson13(int a, int b){
	int j;
	float suma=FX[a]+FX[b];
	for(j=a+1; j<=b-1; j++){
		if(j % 2 == 0){
			suma += (2*FX[j]);
		}else{
			suma += (4*FX[j]);
		}
	}
	return (H/3)*suma;
}

float simpson38(int a, int b){
	int k;
	float sum=FX[a]+FX[b];
	for(k=a+1; k<=b-1; k++){
		if(k % 3 == 0){
			sum += (2*FX[k]);
		}else{
			sum += (3*FX[k]);
		}
	}
	return ((H*3)/8)*sum;
}

void reset(){
	int i;
	for(i=0;i<40;i++){
		X[i]=0;
		FX[i]=0;
		result[i]=0;
	}
	float H;
	numValores2=0;
	numPaneles=0;
	integral=0;
}

void deriva(){
	int opc,a,b;
	printf("\n\nOpciones: \n\t 1) Derivar todos los valores de la tabla \n\t 2) Derivar en un intervalo espec�fico \n\t");
	scanf("%d",&opc);
	
	if(opc == 1){
		//a=0;
		//b=numPaneles
		//derivaci�n en todos
		derivarInt(0,numPaneles);
	}else{
		if(opc == 2){
			printf("\n\n Indica la 'i' del primer valor del intervalo (?,b) : ");
			scanf("%d",&a);
			printf("\n\n Indica la 'i' del segundo valor del intervalo (%.4f,?) : ",X[a]);
			scanf("%d",&b);
			if(a < b && (b-a)>1){
				derivarInt(a,b);
			}
		
		}
	}
}

void derivarInt(int a, int b){
	system("cls");
	int i;
		//condici�n a < b
		//separados al menos por uno
	result[a] = (1/H)*((FX[a+1]-FX[a])-(0.5)*(FX[a+2]- 2*FX[a+1]+ FX[a]));//progresivo
	//result[b] = (1/(2*H))*(FX[b-2]-(4*FX[b-1])+(3*FX[b]));//regresivo
	result[b] = (1/H)*(FX[b]-FX[b-1]);
	
	for(i=a+1; i<=b-1; i++){//centralizadas
		result[i]= (1/(2*H))*(FX[i+1]-FX[i-1]);
	}
	
	printf("    --------------------TABLA-------------------");
	printf("\n i |        X        |     F(x)    |     F'(x)    ");
	printf("\n    --------------------------------------------------");
	for(i=a; i<=b; i++){
		printf("\n %d |      %.4f     |    %.4f     |    %.4f     ",i,X[i],FX[i],result[i]);
	} 
	printf("\n\n");
	system("pause");
	
}

int entregaTres(){
	setlocale(LC_ALL,"");
	
	int i;
	int resp,opc,opc2;
	float a,b; //Intervalo del dominio
	
	system("cls");
	
	printf("\t\t\tTercer Programa de M�todos Num�ricos 2\n\n");
	printf("\n\n\t 1)Ingresar tabla de valores \n\n\t 2)Salir \n\n\t");
	scanf("%d",&opc2);
	
	while(opc2 != 2){
		reset();
		system("cls");
		
		do
		{
		printf("Ingrese el Limite Inferior\n"); ///////////Limite inferior de la integral 
		scanf("%f",&a);
		printf("El Valor Es Correcto?\nSi = 1\nNo = 2\n");
		scanf("%d",&resp);
		}while(resp!=1);
		system("cls");
		
		do
		{
		printf("Ingrese el Limite Superior\n"); //////////Limite superior de La integral
		scanf("%f",&b);
		printf("El Valor Es Correcto?\nSi = 1\nNo = 2\n");
		scanf("%d",&resp);
		}while(resp!=1);
		system("cls");
		
		do
		{
		printf("Ingrese La H: "); ///////////se pide la H
		scanf("%f",&H);
		printf("El Valor Es Correcto?\nSi = 1\nNo = 2\n");
		scanf("%d",&resp);
		}while(resp!=1);
		system("cls");
		
		numPaneles=(b-a)/H;
		numValores2=numPaneles+1;
		
		for(i=0; i<numValores2; i++){
			X[i] = a + (i*H);
			do
			{
			printf("Ingrese el valor de F(%.4f): ",X[i]); 
			scanf("%f",&FX[i]);
			printf("El Valor Es Correcto?\nSi = 1\nNo = 2\n");
			scanf("%d",&resp);
			}while(resp!=1);
			system("cls");
		}
		
		printf("    --------------TABLA-------------");
			printf("\n i |        X        |     F(x)    ");
			printf("\n    --------------------------------");
			for(i=0; i<numValores2; i++){
				printf("\n %d |      %.4f     |    %.4f     ",i,X[i],FX[i]);
			} 
			printf("\n\nOperaciones: \n\t1)Integrar \n\t2)Derivar \n\t3)Salir \n");
			scanf("%d",&opc);
	
		while(opc != 3){
			
			system("cls");
			
//			printf("    --------------TABLA-------------");
//			printf("\n i |        X        |     F(x)    ");
//			printf("\n    --------------------------------");
//			for(i=0; i<numValores2; i++){
//				printf("\n %d |      %.4f     |    %.4f     ",i,X[i],FX[i]);
//			} 
//			printf("\n\nOperaciones: \n\t1)Integrar \n\t2)Derivar \n\t3)Salir \n");
//			scanf("%d",&opc);
			
			if(opc==1){
				integra();
				printf("\n\n La integral en el intervalo es %f",integral);
				printf("\n\n");
				system("pause");
			}else{
				if(opc==2){
					deriva();
					printf("\n\n");
					system("pause");
				}
			}
			
			system("cls");
			
			printf("    --------------TABLA-------------");
			printf("\n i |        X        |     F(x)    ");
			printf("\n    --------------------------------");
			for(i=0; i<numValores2; i++){
				printf("\n %d |      %.4f     |    %.4f     ",i,X[i],FX[i]);
			} 
			printf("\n\nOperaciones: \n\t1)Integrar \n\t2)Derivar \n\t3)Salir \n");
			scanf("%d",&opc);
			
			
		}
		
		
		system("cls");
		printf("\t\t\tTercer Programa de M�todos Num�ricos 2\n\n");
		printf("\n\n\t 1)Ingresar tabla de valores \n\n\t 2)Salir \n\n\t");
		scanf("%d",&opc2);
}
	
	return 0;
}
