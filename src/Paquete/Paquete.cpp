#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <locale.h>
#include "entregaUno.cpp"
#include "entregaDos.cpp"
#include "entregaTres.cpp"


int main(){
	setlocale(LC_ALL,"");
	
	
	int opc=0, opc2=0;
	
	do{
	
	system("cls");
	printf("\t UNIVERSIDAD NACIONAL AUT�NOMA DE M�XICO");
	printf("\n\t Facultad de Estudios Superiores Acatl�n");
	printf("\n\n\t Matem�ticas Aplicadas y Computaci�n");
	printf("\n\n\t M�todos Num�ricos 2 | Grupo: 2401");
	printf("\n\t Profesora Teresa Carrillo Ram�rez");
	printf("\n\n\t Paquete Final de programaci�n.");
	printf("\n\n");
	system("pause");
	system("cls");
	
	printf("Escoge una opci�n: \n");
	printf("\t 1 - Sistemas de ecuaciones no lineales.\n");
	printf("\t 2 - Interpolaci�n y ajuste de curvas. \n");
	printf("\t 3 - Diferenciaci�n e integraci�n. \n");
	printf("\t 4 - Salir. \n");
	
	scanf("%d",&opc);
	
	switch(opc){
		case 1:
			entregaUno();
			break;
		
		case 2:
			entregaDos();
			break;
			
		case 3:
			entregaTres();
			break;
	}
	
	
	
	}while(opc!=4);
	
	system("cls");
	
	printf("\n\tIntegrantes: ");
	printf("\n\n\t\t- Alc�ntara Sarmiento Fernando \n\t\t- Del Castillo Flores Ana Karen \n\t\t- Flores Zenteno Alfonso \n\t\t- Gonz�lez Briones Miguel Ariel");
	printf("\n\n");
	system("pause");
	
	return 0;
}
