#include <stdio.h>
#include <math.h>


class nrf2_1 {
	private:
		double x, y;
		double matrizf21[2][2];
		double evalf21[2];
		double minversa[4];
		double determ;


	public:
		
		void setval(double v, double w){
		x=v;
		y=w;
		};
		
		void jacobiana(){
			matrizf21[0][0]=(2*x)+y;
			matrizf21[0][1]=x;
			matrizf21[1][0]=3*pow(y,2);
			matrizf21[1][1]=6*(x*y)+1;
		}
		
		void evaluar(){
			evalf21[0]= pow(x,2)+(x*y)-10;
			evalf21[1]= y+(3*(x*pow(y,2)))-50;
		};
		
		double determinante(){
			determ=(matrizf21[0][0]*matrizf21[1][1])-(matrizf21[0][1]*matrizf21[1][0]);
			return determ;
		}
		
		double *getjacob_inv(){
			int l,j,k=0;
			determinante();
			minversa[0]=matrizf21[1][1]/determ;
			minversa[1]=-1*matrizf21[0][1]/determ;
			minversa[2]=-1*matrizf21[1][0]/determ;
			minversa[3]=matrizf21[0][0]/determ;
			
			return &minversa[0];
		};
		
		double *geteval(){
			return &evalf21[0];
			};
			
		void imprime(){
			int l,j,k=0,i=0;
			
			printf("\n|Jacobiana^-1 | F(X^k-1)|\n");
			for(l=0;l<2;l++){
				printf("\n");
				for(j=0;j<2;j++){
					printf("| %.9lf |",minversa[k]);
					k++;
				}
				printf(" | %.9lf |\n",evalf21[l]);
			}
			
		}
		
};

