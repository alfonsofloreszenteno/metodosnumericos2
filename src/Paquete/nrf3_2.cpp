#include <stdio.h>
#include <math.h>

class nrf3_2 {
	private:
		double x, y,z;
		double matrizf32[3][3];
		double evalf32[3];
		double minversa[9];
		double cofact[3][3];
		double adjt[3][3];
		double determ;

	public:
		
		void setval(double v,double w,double h){
		x=v;
		y=w;
		z=h;
		};
		
		void jacobiana(){
			matrizf32[0][0]=(2*x)-4;
			matrizf32[0][1]=2*y;
			matrizf32[0][2]=0;
			matrizf32[1][0]=(2*x)-1;
			matrizf32[1][1]=-12;
			matrizf32[1][2]=0;
			matrizf32[2][0]=(6*x)-12;
			matrizf32[2][1]=2*y;
			matrizf32[2][2]=-6*z;
		};
		
		void evaluar(){
			evalf32[0]= pow(x,2)-(4*x)+pow(y,2);
			evalf32[1]= pow(x,2)-x -(12*y)+1;
			evalf32[2]= (3*pow(x,2))-(12*x)+pow(y,2)-(3*pow(z,2))+8;
		};
		
		double determinante(){
			double sum, rest;
			sum =(matrizf32[0][0]*matrizf32[1][1]*matrizf32[2][2])+(matrizf32[1][0]*matrizf32[2][1]*matrizf32[0][2])+(matrizf32[2][0]*matrizf32[0][1]*matrizf32[1][2]);
			rest=(matrizf32[0][2]*matrizf32[1][1]*matrizf32[2][0])+(matrizf32[1][2]*matrizf32[2][1]*matrizf32[0][0])+(matrizf32[2][2]*matrizf32[0][1]*matrizf32[1][0]);
			determ=sum-rest;
			return determ;
		}
		
		void cofactores(){
			cofact[0][0]=(matrizf32[1][1]*matrizf32[2][2])-(matrizf32[1][2]*matrizf32[2][1]);
			cofact[0][1]=-1*((matrizf32[1][0]*matrizf32[2][2])-(matrizf32[1][2])*matrizf32[2][0]);
			cofact[0][2]=(matrizf32[1][0]*matrizf32[2][1])-(matrizf32[1][1]*matrizf32[2][0]);
			cofact[1][0]=-1*((matrizf32[0][1]*matrizf32[2][2])-(matrizf32[0][2]*matrizf32[2][1]));
			cofact[1][1]=(matrizf32[0][0]*matrizf32[2][2])-(matrizf32[0][2]*matrizf32[2][0]);
			cofact[1][2]=-1*((matrizf32[0][0]*matrizf32[2][1])-(matrizf32[0][1]*matrizf32[2][0]));
			cofact[2][0]=(matrizf32[0][1]*matrizf32[1][2])-(matrizf32[0][2]*matrizf32[1][1]);
			cofact[2][1]=-1*((matrizf32[0][0]*matrizf32[1][2])-(matrizf32[0][2]*matrizf32[1][0]));
			cofact[2][2]=(matrizf32[0][0]*matrizf32[1][1])-(matrizf32[0][1]*matrizf32[1][0]);
		}
		
		void adjunta(){
			adjt[0][0]=cofact[0][0];
			adjt[0][1]=cofact[1][0];
			adjt[0][2]=cofact[2][0];
			adjt[1][0]=cofact[0][1];
			adjt[1][1]=cofact[1][1];
			adjt[1][2]=cofact[2][1];
			adjt[2][0]=cofact[0][2];
			adjt[2][1]=cofact[1][2];
			adjt[2][2]=cofact[2][2];
		}
		
		double *getjacob_inv(){
			determinante();
			cofactores();
			adjunta();
			minversa[0]=adjt[0][0]/determ;
			minversa[1]=adjt[0][1]/determ;
			minversa[2]=adjt[0][2]/determ;
			minversa[3]=adjt[1][0]/determ;
			minversa[4]=adjt[1][1]/determ;
			minversa[5]=adjt[1][2]/determ;
			minversa[6]=adjt[2][0]/determ;
			minversa[7]=adjt[2][1]/determ;
			minversa[8]=adjt[2][2]/determ;
			return &minversa[0];
		};
		
		double *geteval(){
			return &evalf32[0];
			};
		
		void imprime(){
			int l,j,k=0;
			
			printf("\n             |Jacobiana^-1              | F(X^k-1)|\n");
			for(l=0;l<3;l++){
				printf("\n");
				for(j=0;j<3;j++){
					printf("| %.9lf |",minversa[k]);
					k++;
				}
				printf(" | %.9lf |\n",evalf32[l]);
			}
			
		};
};

