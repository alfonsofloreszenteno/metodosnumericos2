#include <stdio.h>
#include <math.h>


class nrf2_2 {
	private:
		double x, y;
		double matrizf22[2][2];
		double evalf22[2];
		double minversa[4];
		double determ;

	public:
		
		void setval(double v,double w){
		x=v;
		y=w;
		};
		
		void jacobiana(){
			matrizf22[0][0]=2*x;
			matrizf22[0][1]=2*y;
			matrizf22[1][0]=-1*exp(x);
			matrizf22[1][1]=-2;
		}
		
		void evaluar(){
			evalf22[0]= pow(x,2)+pow(y,2)-9;
			evalf22[1]= -1*exp(x)-(2*y)-3;
		};
		
		double determinante(){
			determ=(matrizf22[0][0]*matrizf22[1][1])-(matrizf22[0][1]*matrizf22[1][0]);
			return determ;
		}
		
		double *getjacob_inv(){
			determinante();
			minversa[0]=matrizf22[1][1]/determ;
			minversa[1]=-1*matrizf22[0][1]/determ;
			minversa[2]=-1*matrizf22[1][0]/determ;
			minversa[3]=matrizf22[0][0]/determ;
			return &minversa[0];
		};
		
		double *geteval(){
			return &evalf22[0];
			};
			
		void imprime(){
			int l,j,k=0;
			
			printf("\n|Jacobiana^-1 | F(X^k-1)|\n");
			for(l=0;l<2;l++){
				printf("\n");
				for(j=0;j<2;j++){
					printf("| %.9lf |",minversa[k]);
					k++;
				}
				printf(" | %.9lf |\n",evalf22[l]);
			}
			
		};
		
};
